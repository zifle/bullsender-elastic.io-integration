var qs = require("querystring");
var HttpComponent = require("elasticio-node").HttpComponent;
var helper = require('../helpers.js');

var queryUrl = 'https://api.bullsender.com/Subscriber/Add';

exports.process = AddSubscriber;
exports.getUserLists = helper.getUserLists;
exports.getMetaModel = helper.getListsCustomfields;

function AddSubscriber(msg, cfg) {

    var self = this;

    var body = msg.body;

    var queryParams = {
        "api_key": cfg.api_key,
        "list_id": cfg.list,
        "resubscribe": true
    };

    for (var i in body) {
        if (body.hasOwnProperty(i) && queryParams[i] === undefined) {
            queryParams[i] = body[i];
        }
    }

    var options = {
        url: queryUrl + '?' + qs.stringify(queryParams),
        json: true
    };

    function onSuccess(response, body) {
        var success;
        if (response.statusCode == 200) {
            // Woo, we've successfully added the subscriber!
            success = true;
        } else if (response.statusCode == 500) {
            // Boo, something went wrong! But it may not be all bad!
            switch (body.code) {
                case 2: // User already subscribed
                    success = true; // We'll count this as a success, since nothing went wrong
                    break;
                case 0: // Try again later (Unknown error)
                default: // Unknown error
                    success = false;
            }
        }
        self.emit('data', {success:success});
    }

    new HttpComponent(this).success(onSuccess).post(options);
}